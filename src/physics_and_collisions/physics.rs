use crate::common::animated_sprite_state::CollisionLayer;
use bevy::prelude::*;
use avian2d::prelude::*;

#[derive(Bundle)]
pub struct SpritePhysicsAndCollisionsBundle {
    pub rigid_body: RigidBody,
    pub gravity_scale: GravityScale,
    pub velocity: LinearVelocity,
    pub collider_density: ColliderDensity,
    pub locked_axes: LockedAxes,
    pub collider: Collider,
    pub collision_layer: CollisionLayer,
    pub friction: Friction,
    pub restitution: Restitution,
    pub linear_damping: LinearDamping,
    pub dominance: Dominance,
    pub mass: Mass,
}

impl Default for SpritePhysicsAndCollisionsBundle {
    fn default() -> Self {
        Self {
            rigid_body: RigidBody::Dynamic,
            gravity_scale: GravityScale::from(100.0),
            locked_axes: LockedAxes::new().lock_rotation(),
            collider: Default::default(),
            collision_layer: CollisionLayer::Floor,
            velocity: Default::default(),
            collider_density: ColliderDensity(0.0),
            friction: Friction::from(0.0),
            restitution: Restitution::ZERO,
            linear_damping: LinearDamping::from(0.0),
            dominance: Dominance::default(),
            mass: Mass(100.0),
        }
    }
}

pub struct PhysicsPlugin;

impl Plugin for PhysicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((PhysicsPlugins::default(), PhysicsDebugPlugin::default()))
            .insert_resource(Gravity(Vec2::NEG_Y * 9.81));
    }
}
