use bevy::prelude::*;

#[derive(PartialEq, Eq, Clone, Copy, Component, Debug)]
pub enum DrawLayer {
    Background0 = 0,
    Background1 = 1,
    Background2 = 2,
    Object = 3,
    Overlay = 4,
}

impl DrawLayer {
    pub fn from_i32(layer_index: i32) -> DrawLayer {
        match layer_index {
            0 => DrawLayer::Background0,
            1 => DrawLayer::Background1,
            2 => DrawLayer::Background2,
            3 => DrawLayer::Object,
            4 => DrawLayer::Overlay,
            _ => panic!("Unknown layer index: {}", layer_index),
        }
    }
}

pub fn get_z_index(game_layer: DrawLayer) -> f32 {
    game_layer as i32 as f32 / 10.0
}