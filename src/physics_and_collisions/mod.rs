use bevy::prelude::{App, Plugin};

pub mod collisions;
pub mod game_layer;
pub mod physics;

pub struct CollisionsAndPhysicsPlugin;

impl Plugin for CollisionsAndPhysicsPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(physics::PhysicsPlugin)
            .add_plugins(collisions::CollisionsPlugin);
    }
}
