use crate::{
    common::animated_sprite_state::CollisionLayer,
    physics_and_collisions::physics::SpritePhysicsAndCollisionsBundle,
};

use bevy::prelude::*;
use avian2d::{math::Scalar, prelude::*};
use micro_banimate::definitions::SpriteAnimationBundle;

use super::state::{EnemyBundle, EnemyState, EnemyType};

pub fn create_enemy_bundle(
    enemy_type: EnemyType,
    asset_server: &Res<AssetServer>,
    start_position: &Vec3,
    direction: &Vec2,
    tile_size: &Vec2,
    (columns, rows): (usize, usize),
    collider: Collider,
    mass: Option<Scalar>,
) -> (
    TransformBundle,
    //VisibilityBundle,
    SpriteAnimationBundle,
    EnemyBundle,
    SpritePhysicsAndCollisionsBundle,
) {
    let enemy_name = enemy_type.to_string();
    let enemy_state = match enemy_type {
        EnemyType::Skeleton => EnemyState::skeleton(),
        EnemyType::Zombie => EnemyState::zombie(),
        EnemyType::Ghost => EnemyState::ghost(),
    };
    let enemy_name_cloned = enemy_name.clone();

    (
        TransformBundle::from_transform(Transform::from_translation(*start_position)),
        //VisibilityBundle::default(),
        SpriteAnimationBundle::new(
            enemy_name,
            asset_server.load(String::from("anims/enemies/") + enemy_name_cloned.as_str() + ".anim.json"),
        ),
        EnemyBundle {
            state: enemy_state,
        },
        SpritePhysicsAndCollisionsBundle {
            rigid_body: RigidBody::Dynamic,
            gravity_scale: GravityScale::default(),
            locked_axes: LockedAxes::new().lock_rotation(),
            collider: collider.clone(),
            //active_events: ActiveEvents::COLLISION_EVENTS,
            collision_layer: CollisionLayer::Enemy,
            velocity: LinearVelocity::from(*direction),
            //collider_debug_color: ColliderDebugColor(Color::GREEN),
            mass: if let Some(mass) = mass { Mass(mass) } else { Mass(100.0) },
            //ccd: Ccd::disabled(),
            linear_damping: LinearDamping::default(),
            ..Default::default()
        },
    )
}

pub fn create_skeleton_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec3,
) -> (
    TransformBundle,
    //VisibilityBundle,
    SpriteAnimationBundle,
    EnemyBundle,
    SpritePhysicsAndCollisionsBundle,
) {
    create_enemy_bundle(
        EnemyType::Skeleton,
        asset_server,
        start_position,
        &Vec2::ZERO,
        &Vec2::new(46.0, 46.0),
        (26, 13),
        Collider::capsule(6.0, 16.0),
        None
    )
}

pub fn create_zombie_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec3,
) -> (
    TransformBundle,
    //VisibilityBundle,
    SpriteAnimationBundle,
    EnemyBundle,
    SpritePhysicsAndCollisionsBundle,
) {
    create_enemy_bundle(
        EnemyType::Zombie,
        asset_server,
        start_position,
        &Vec2::ZERO,
        &Vec2::new(46.0, 46.0),
        (26, 13),
        Collider::capsule(6.0, 16.0),
        None
    )
}

pub fn create_ghost_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec3,
) -> (
    TransformBundle,
    //VisibilityBundle,
    SpriteAnimationBundle,
    EnemyBundle,
    SpritePhysicsAndCollisionsBundle,
) {
    create_enemy_bundle(
        EnemyType::Ghost,
        asset_server,
        start_position,
        &Vec2::ZERO,
        &Vec2::new(46.0, 46.0),
        (26, 13),
        Collider::capsule(6.0, 16.0),
        None
    )
}
