use crate::{
    common::animated_sprite_state::{Action, Actionable, AnimationType, CollisionState},
    player::state::PlayerState,
};

use bevy::{prelude::*, transform::components::Transform};
use avian2d::prelude::LinearVelocity;
use micro_banimate::definitions::{AnimationMode, AnimationStatus};

use super::state::EnemyState;

pub fn update(
    mut commands: Commands,
    _time: Res<Time>,
    mut set: ParamSet<(
        Query<(&Transform, &mut PlayerState)>,
        Query<(
            &mut LinearVelocity,
            &Transform,
            &mut EnemyState,
            &mut Visibility,
            &mut AnimationStatus,
            &mut AnimationMode,
            &mut Sprite,
            Entity,
        )>,
    )>,
) {
    let mut player_translation = Vec3::ZERO;
    let mut player_collision_state = CollisionState::NoColliding;
    if let Ok((player_transform, player_state)) = set.p0().get_single() {
        player_translation = player_transform.translation;
        player_collision_state = player_state.state.current_collision_state.clone();
    }
    set.p1().iter_mut().for_each(
        |(
            mut velocity,
            transform,
            mut enemy_state,
            mut _visibility,
            mut animation_status,
            mut _animation_mode,
            mut sprite,
            entity,
        )| {
            if enemy_state
                .state
                .current_collision_state
                .contains(CollisionState::CollidingDeadlyObject)
                || enemy_state
                    .state
                    .current_collision_state
                    .contains(CollisionState::CollidingPlayer)
            {
                /*let previous_animation = animation_status.active_name.clone();
                animation_status.start_animation(AnimationType::Die.to_string());
                *animation_mode = AnimationMode::OnceThenPlay(previous_animation);*/
                player_collision_state &= !CollisionState::CollidingEnemy;
                commands.entity(entity).despawn();
                return;
            }

            if !enemy_state.state.check_current_action(Action::Grounded) {
                enemy_state.state.add_action(Action::Grounded);
            }

            let player_direction = player_translation - transform.translation;
            velocity.x = player_direction.x.signum() * enemy_state.state.max_velocity.x;
            if velocity.x.abs() > enemy_state.state.min_velocity.x {
                animation_status.start_or_continue(AnimationType::Walk.to_string());
            } else {
                animation_status.start_or_continue(AnimationType::Idle.to_string());
            }

            sprite.flip_x = velocity.x < 0.0;
        },
    );
    if let Ok((_, mut player_state)) = set.p0().get_single_mut() {
        if player_collision_state != player_state.state.current_collision_state {
            player_state.state.current_collision_state = player_collision_state;
        }
    }
}

pub fn despawn_current_enemies(
    commands: &mut Commands,
    enemies_query: &Query<(Entity, &EnemyState)>,
) {
    enemies_query.iter().for_each(|(entity, _)| {
        commands.entity(entity).despawn_recursive();
    });
}
