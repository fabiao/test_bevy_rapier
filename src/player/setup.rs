use crate::{
    common::{
        animated_sprite_state::CollisionLayer,
        game_layer::{get_z_index, DrawLayer},
    },
    physics_and_collisions::physics::SpritePhysicsAndCollisionsBundle,
};

use bevy::{color::palettes::{css::*, tailwind::CYAN_100}, prelude::*};
use avian2d::{math::Scalar, prelude::*};
use micro_banimate::{definitions::DirectionalAnimationBundle, directionality::Directionality};

use super::state::{
    PlayerAnimationSet, PlayerBundle, PlayerState, WeaponBundle, WeaponState, WeaponType,
};

pub fn create_player_bundle(
    asset_server: &Res<AssetServer>,
    position: &Vec3,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    PlayerBundle,
    SpritePhysicsAndCollisionsBundle,
) {
    let mut player_state = PlayerState::default();

    player_state.animation_sets.insert(
        PlayerAnimationSet::Naked,
        asset_server.load(
            String::from("anims/player/")
                + PlayerAnimationSet::Naked.to_string().as_str()
                + ".anim.json",
        ),
    );
    player_state.animation_sets.insert(
        PlayerAnimationSet::IronArmor,
        asset_server.load(
            String::from("anims/player/")
                + PlayerAnimationSet::IronArmor.to_string().as_str()
                + ".anim.json",
        ),
    );
    player_state.animation_sets.insert(
        PlayerAnimationSet::GoldArmor,
        asset_server.load(
            String::from("anims/player/")
                + PlayerAnimationSet::GoldArmor.to_string().as_str()
                + ".anim.json",
        ),
    );
    player_state.current_animation_set = PlayerAnimationSet::IronArmor;

    player_state.current_weapon_type = WeaponType::Spear;

    let collider = Collider::capsule(18.0, 12.0);

    (
        SpriteBundle {
            sprite: Sprite {
                color: Color::Srgba(RED),
                ..default()
            },
            transform: Transform::from_translation(*position),
            visibility: Visibility::Visible,
            ..default()
        },
        DirectionalAnimationBundle::with_direction(
            String::from("idle"),
            player_state.get_current_animation_set_handle(),
            Directionality::Right,
        ),
        PlayerBundle {
            state: player_state,
        },
        SpritePhysicsAndCollisionsBundle {
            rigid_body: RigidBody::Dynamic,
            collider: collider.clone(),
            collision_layer: CollisionLayer::Player,
            collider_density: ColliderDensity(1.0),
            ..Default::default()
        },
    )
}

pub fn create_weapon_bundle(
    weapon_type: WeaponType,
    asset_server: &Res<AssetServer>,
    start_position: &Vec2,
    direction: &Vec2,
    collider: Collider,
    density: Scalar,
    color: Color,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    WeaponBundle,
    SpritePhysicsAndCollisionsBundle,
    Sensor,
) {
    let weapon_name = weapon_type.to_string();
    let mut weapon_state = match weapon_type {
        WeaponType::Spear => WeaponState::spear(),
        WeaponType::Knife => WeaponState::knife(),
        WeaponType::Axe => WeaponState::axe(),
        WeaponType::Fire => WeaponState::fire(),
        WeaponType::Sword => WeaponState::sword(),
    };
    let max_velocity = weapon_state.max_velocity;
    let weapon_direction = weapon_state.direction;
    let final_direction = Vec2::new(
        /*weapon_direction.x **/ direction.x.signum(),
        weapon_direction.y,
    ) * max_velocity;
    weapon_state.direction = *direction;

    (
        SpriteBundle {
            sprite: Sprite {
                color: color,
                ..default()
            },
            transform: Transform::from_xyz(
                start_position.x,
                start_position.y,
                get_z_index(DrawLayer::Object),
            ),
            visibility: Visibility::Visible,
            ..Default::default()
        },
        DirectionalAnimationBundle::new(
            weapon_name,
            asset_server.load("anims/player/weapons.anim.json"),
        ),
        WeaponBundle {
            state: weapon_state,
        },
        SpritePhysicsAndCollisionsBundle {
            rigid_body: RigidBody::Dynamic,
            collider: collider.clone(),
            collision_layer: CollisionLayer::Weapon,
            velocity: LinearVelocity::from(final_direction),
            collider_density: ColliderDensity(density),
            dominance: Dominance(5),
            ..Default::default()
        },
        Sensor,
    )
}

pub fn create_spear_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec2,
    direction: &Vec2,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    WeaponBundle,
    SpritePhysicsAndCollisionsBundle,
    Sensor,
) {
    create_weapon_bundle(
        WeaponType::Spear,
        asset_server,
        start_position,
        direction,
        Collider::rectangle(21.0, 5.0),
        30.0,
        Color::Srgba(ALICE_BLUE),
    )
}

pub fn create_knife_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec2,
    direction: &Vec2,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    WeaponBundle,
    SpritePhysicsAndCollisionsBundle,
    Sensor,
) {
    create_weapon_bundle(
        WeaponType::Knife,
        asset_server,
        start_position,
        direction,
        Collider::rectangle(13.0, 5.0),
        20.0,
        Color::Srgba(CYAN_100),
    )
}

pub fn create_axe_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec2,
    direction: &Vec2,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    WeaponBundle,
    SpritePhysicsAndCollisionsBundle,
    Sensor,
) {
    create_weapon_bundle(
        WeaponType::Axe,
        asset_server,
        start_position,
        direction,
        Collider::rectangle(18.0, 18.0),
        20.0,
        Color::Srgba(MAROON),
    )
}

pub fn create_fire_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec2,
    direction: &Vec2,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    WeaponBundle,
    SpritePhysicsAndCollisionsBundle,
    Sensor,
) {
    create_weapon_bundle(
        WeaponType::Fire,
        asset_server,
        start_position,
        direction,
        Collider::rectangle(11.0, 11.0),
        20.0,
        Color::Srgba(TEAL),
    )
}

pub fn create_sword_bundle(
    asset_server: &Res<AssetServer>,
    start_position: &Vec2,
    direction: &Vec2,
) -> (
    SpriteBundle,
    DirectionalAnimationBundle,
    WeaponBundle,
    SpritePhysicsAndCollisionsBundle,
    Sensor,
) {
    create_weapon_bundle(
        WeaponType::Sword,
        asset_server,
        start_position,
        direction,
        Collider::rectangle(24.0, 24.0),
        0.0,
        Color::Srgba(ANTIQUE_WHITE),
    )
}
