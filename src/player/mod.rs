pub mod setup;
pub mod state;
pub mod update;

use self::{update::{update_weapons, trigger_weapons}, state::ShootWeaponEvent};

use super::player::update::update as player_update;
use bevy::prelude::*;

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app
        .add_event::<ShootWeaponEvent>()
        .add_systems(Update, (player_update, trigger_weapons, update_weapons));
    }
}
