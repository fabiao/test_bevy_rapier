use bevy::prelude::*;
use bevy::color::palettes::css::*;

use avian2d::prelude::*;

use crate::{common::animated_sprite_state::CollisionLayer, player::setup::create_player_bundle};

pub fn create_map(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut texture_atlas_layouts: ResMut<Assets<TextureAtlasLayout>>,
) {
    // Ground
    let position = Vec3::ZERO;
    let mut local_tranform = Transform::from_translation(position);
    local_tranform.scale = Vec2::new(8000.0, 20.0).extend(1.0);

    {
        commands.spawn((
            SpriteBundle {
                sprite: Sprite {
                    color: Color::Srgba(YELLOW),
                    ..default()
                },
                transform: local_tranform,
                ..default()
            },
            RigidBody::Static,
            CollisionLayer::Floor,
            Collider::rectangle(1.0, 1.0),
        ));
    }

    {
        for i in -20..0 {
            let x = i as f32 * 100.0;
            let y = ((i % 10) as f32).abs() * 52.0;
            let position = Vec3::new(x, y, 0.0);
            let mut local_tranform = Transform::from_translation(position);
            local_tranform.scale = Vec2::new(200.0, 20.0).extend(1.0);

            commands.spawn((
                SpriteBundle {
                    sprite: Sprite {
                        color: Color::Srgba(PURPLE),
                        ..default()
                    },
                    transform: local_tranform,
                    ..default()
                },
                RigidBody::Static,
                CollisionLayer::Floor,
                Collider::rectangle(1.0, 1.0),
            ));
        }
    }

    // Ladders
    for i in -20..20 {
        let position = Vec3::new(200.0 * i as f32, 200.0, 0.0);

        let mut local_tranform = Transform::from_translation(position);
        local_tranform.scale = Vec2::new(60.0, 400.0).extend(1.0);

        {
            commands.spawn((
                SpriteBundle {
                    sprite: Sprite {
                        color: Color::Srgba(GREEN),
                        ..default()
                    },
                    transform: local_tranform,
                    ..default()
                },
                Sensor,
                CollisionLayer::Ladder,
                Collider::rectangle(1.0, 1.0),
            ));
        }
    }

    // Player
    let position = Vec3::new(-100.0, 560.0, 0.0);
    log::info!("Spawn player at position {:?}", position);
    let bundle = create_player_bundle(&asset_server, &position);
    commands.spawn(bundle);

    // Enemies
    /*let mut position = Vec3::new(800.0, 3.0, 0.0);
    log::info!("Spawn skeleton at position {:?}", position);
    let bundle = create_skeleton_bundle(
        &asset_server,
        &position,
    );
    commands.spawn(bundle);

    position.x += 400.0;
    log::info!("Spawn zombie at position {:?}", position);
    let bundle = create_zombie_bundle(
        &asset_server,
        &position,
    );
    commands.spawn(bundle);

    position.x += 400.0;
    log::info!("Spawn ghost at position {:?}", position);
    let bundle = create_ghost_bundle(
        &asset_server,
        &position,
    );
    commands.spawn(bundle);*/
}
