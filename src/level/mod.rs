use bevy::prelude::{Plugin, Startup};

use self::tiled::create_map;

pub mod tiled;

#[derive(Default)]
pub struct LevelPlugin;

impl Plugin for LevelPlugin {
    fn build(&self, app: &mut bevy::prelude::App) {
        app
            .add_systems(Startup, create_map);
    }
}