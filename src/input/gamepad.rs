use super::state::{ButtonState, InputState};
use bevy::{
    app::AppExit,
    input::{
        gamepad::{GamepadConnection, GamepadConnectionEvent},
        Axis, ButtonInput,
    },
    prelude::*,
    utils::HashSet,
};

#[derive(Resource, Clone, Debug, Reflect)]
#[reflect(Resource)]
pub struct GamepadLobby {
    gamepads: HashSet<Gamepad>,
    min_speed: f32,
    max_speed: f32,
    threshold_drift_speed: f32,
}

impl Default for GamepadLobby {
    fn default() -> Self {
        GamepadLobby {
            gamepads: HashSet::default(),
            min_speed: 0.0,
            max_speed: 1.0,
            threshold_drift_speed: 0.25,
        }
    }
}

pub fn connection(
    mut lobby: ResMut<GamepadLobby>,
    mut gamepad_event: EventReader<GamepadConnectionEvent>,
) {
    for event in gamepad_event.read() {
        match &event.connection {
            GamepadConnection::Connected(_gamepad_info) => {
                lobby.gamepads.insert(event.gamepad);
            }
            GamepadConnection::Disconnected => {
                lobby.gamepads.remove(&event.gamepad);
            }
        }
    }
}

pub fn check(
    mut app_exit_events: ResMut<Events<AppExit>>,
    lobby: Res<GamepadLobby>,
    button_inputs: Res<ButtonInput<GamepadButton>>,
    //button_axes: Res<Axis<GamepadButton>>,
    axes: Res<Axis<GamepadAxis>>,
    mut input_state: ResMut<InputState>,
) {
    for gamepad in lobby.gamepads.iter() {
        if button_inputs.pressed(GamepadButton::new(*gamepad, GamepadButtonType::Start)) {
            app_exit_events.send(AppExit::Success);
            return;
        }

        if let Some(x) = axes.get(GamepadAxis::new(*gamepad, GamepadAxisType::LeftStickX)) {
            let x = x / 3.0;
            if x.abs() > lobby.threshold_drift_speed {
                input_state.axes.x = x.signum() * x.abs().min(lobby.max_speed);
            } else {
                input_state.axes.x = lobby.min_speed;
            }
        }
        if let Some(y) = axes.get(GamepadAxis::new(*gamepad, GamepadAxisType::LeftStickY)) {
            let y = y / 3.0;
            if y.abs() > lobby.threshold_drift_speed {
                input_state.axes.y = y.signum() * y.abs().min(lobby.max_speed);
            } else {
                input_state.axes.y = lobby.min_speed;
            }
        }

        if button_inputs.just_released(GamepadButton::new(*gamepad, GamepadButtonType::DPadUp))
            || button_inputs
                .just_released(GamepadButton::new(*gamepad, GamepadButtonType::DPadDown))
        {
            input_state.arrows.y = lobby.min_speed;
        }
        if button_inputs.just_released(GamepadButton::new(*gamepad, GamepadButtonType::DPadRight))
            || button_inputs
                .just_released(GamepadButton::new(*gamepad, GamepadButtonType::DPadLeft))
        {
            input_state.arrows.x = lobby.min_speed;
        }

        if button_inputs.pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadUp)) {
            input_state.arrows.y = (input_state.arrows.y + input_state.step).min(lobby.max_speed);
            /*if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadUp)) {
                log::info!("Pressing pad up");
            }*/
        }
        if button_inputs.pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadRight)) {
            input_state.arrows.x = (input_state.arrows.x + input_state.step).min(lobby.max_speed);
            /*if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadRight)) {
                log::info!("Pressing pad right");
            }*/
        }
        if button_inputs.pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadDown)) {
            input_state.arrows.y = (input_state.arrows.y - input_state.step).max(-lobby.max_speed);
            /*if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadDown)) {
                log::info!("Pressing pad down");
            }*/
        }
        if button_inputs.pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadLeft)) {
            input_state.arrows.x = (input_state.arrows.x - input_state.step).max(-lobby.max_speed);
            /*if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::DPadLeft)) {
                log::info!("Pressing pad left");
            }*/
        }

        if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::South)) {
            if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::South)) {
                input_state.jump_state = ButtonState::JustPressed;
            } else {
                input_state.jump_state = ButtonState::Pressing;
            }
        } else if button_inputs
            .just_released(GamepadButton::new(*gamepad, GamepadButtonType::South))
        {
            input_state.jump_state = ButtonState::JustReleased;
        }

        if button_inputs.pressed(GamepadButton::new(*gamepad, GamepadButtonType::West)) {
            if button_inputs.just_pressed(GamepadButton::new(*gamepad, GamepadButtonType::West)) {
                input_state.shoot_state = ButtonState::JustPressed;
            } else {
                input_state.shoot_state = ButtonState::Pressing;
            }
        } else if button_inputs.just_released(GamepadButton::new(*gamepad, GamepadButtonType::West))
        {
            input_state.shoot_state = ButtonState::JustReleased;
        }
    }
}