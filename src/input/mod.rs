pub mod gamepad;
pub mod keyboard;
pub mod state;

use self::{
    gamepad::{check as check_gamepad, connection as pad_connection, GamepadLobby},
    keyboard::check as check_keyboard,
    state::InputState,
};
use bevy::prelude::*;

pub struct InputPlugin;

impl Plugin for InputPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(InputState::default())
            .init_resource::<GamepadLobby>()
            .add_systems(PreUpdate, (pad_connection, cleanup_inputs))
            .add_systems(
                Update,
                (check_keyboard, check_gamepad),
            );
    }
}

pub fn cleanup_inputs(mut input_state: ResMut<InputState>) {
    input_state.reset();
}
