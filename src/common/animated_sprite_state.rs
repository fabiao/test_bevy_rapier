use core::fmt;
use std::fmt::Display;

use bevy::{
    prelude::{Component, ReflectComponent, Vec2},
    reflect::Reflect,
    time::Timer,
};
use avian2d::prelude::PhysicsLayer;
use bitflags::bitflags;

pub trait Actionable {
    fn check_current_action(&mut self, action: Action) -> bool;
    fn can_shoot(&self) -> bool;
    fn is_alive(&self) -> bool;
    fn can_move(&self) -> bool;
    fn get_current_actions(&self) -> &Action;
    //fn change_action(&mut self, action: Action);
    fn add_action(&mut self, action: Action);
    fn remove_action(&mut self, action: Action);
    fn check_action(&mut self, action: Action) -> bool;
}

#[derive(Clone, Component, Copy, Default, Debug, Eq, Hash, PartialEq, Reflect)]
#[reflect(Component)]
#[cfg_attr(feature = "serde-serialize", derive(Serialize, Deserialize))]
pub struct Action(u32);

bitflags! {
    impl Action: u32 {
        const None = 0b0000000000;
        const Grounded = 0b0000000001;
        const Jumping = 0b0000000010;
        const Climbing = 0b0000000100;
        const Shooting = 0b0000001000;
        const Falling = 0b0000010000;
        const Rising = 0b0000100000;
        const Dying = 0b0001000000;
        const Dead = 0b0010000000;
        const Crouched = 0b0100000000;
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut displayed_actions = Vec::new();
        if self.intersects(Action::Grounded) {
            displayed_actions.push("Grounded");
        }
        if self.intersects(Action::Jumping) {
            displayed_actions.push("Jumping");
        }
        if self.intersects(Action::Climbing) {
            displayed_actions.push("Climbing");
        }
        if self.intersects(Action::Shooting) {
            displayed_actions.push("Shooting");
        }
        if self.intersects(Action::Falling) {
            displayed_actions.push("Falling");
        }
        if self.intersects(Action::Rising) {
            displayed_actions.push("Rising");
        }
        if self.intersects(Action::Dying) {
            displayed_actions.push("Dying");
        }
        if self.intersects(Action::Dead) {
            displayed_actions.push("Dead");
        }
        if self.intersects(Action::Crouched) {
            displayed_actions.push("Crouched");
        }
        if displayed_actions.len() == 0 {
            displayed_actions.push("None");
        }
        write!(f, "{}", displayed_actions.join("|"))
    }
}

#[derive(Clone, Component, Copy, Default, Debug, Eq, Hash, PartialEq, Reflect)]
#[reflect(Component)]
#[cfg_attr(feature = "serde-serialize", derive(Serialize, Deserialize))]
pub struct CollisionState(u32);

bitflags! {
    impl CollisionState: u32 {
        const NoColliding = 0b00000000;
        const CollidingFloor = 0b00000001;
        const CollidingLadder = 0b00000010;
        const CollidingEnemy = 0b00000100;
        const CollidingDeadlyObject = 0b00001000;
        const CollidingPlayer = 0b00010000;
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Component, Debug)]
pub enum CollisionLayer {
    Floor,
    Ladder,
    Enemy,
    Player,
    Weapon,
}

#[derive(Component, Clone)]
pub struct AnimatedSpriteState {
    pub current_action: Action,
    pub current_collision_state: CollisionState,
    pub velocity_increment: Vec2,
    pub max_velocity: Vec2,
    pub min_velocity: Vec2,
    pub animation_timer: Timer,
    pub animation_speed: f32,
}

impl Actionable for AnimatedSpriteState {
    fn check_current_action(&mut self, action: Action) -> bool {
        return self.current_action.intersects(action);
    }

    fn can_shoot(&self) -> bool {
        self.current_action.intersects(Action::Grounded)
            || self.current_action.intersects(Action::Jumping)
    }

    fn is_alive(&self) -> bool {
        !self.current_action.intersects(Action::Dying | Action::Dead)
    }

    fn can_move(&self) -> bool {
        self.is_alive() && !self.current_action.intersects(Action::Falling)
    }

    fn get_current_actions(&self) -> &Action {
        &self.current_action
    }

    /*fn change_action(&mut self, action: Action) {
        self.current_action = action;
    }*/

    fn add_action(&mut self, action: Action) {
        self.current_action |= action;
    }

    fn remove_action(&mut self, action: Action) {
        self.current_action &= !action;
    }

    fn check_action(&mut self, action: Action) -> bool {
        self.current_action.intersects(action)
    }
}

impl Default for AnimatedSpriteState {
    fn default() -> Self {
        Self {
            current_action: Action::None,
            current_collision_state: CollisionState::NoColliding,
            min_velocity: Vec2::ZERO,
            max_velocity: Vec2::ZERO,
            velocity_increment: Vec2::ZERO,
            animation_timer: Timer::default(),
            animation_speed: 1.0,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum AnimationType {
    None,
    Idle,
    Walk,
    Jump,
    Climb,
    Crouch,
    Fall,
    Rise,
    Die,
    Shoot,
    ShootCrouched,
}

impl fmt::Display for AnimationType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            AnimationType::None => write!(f, "none"),
            AnimationType::Idle => write!(f, "idle"),
            AnimationType::Walk => write!(f, "walk"),
            AnimationType::Jump => write!(f, "jump"),
            AnimationType::Climb => write!(f, "climb"),
            AnimationType::Crouch => write!(f, "crouch"),
            AnimationType::Fall => write!(f, "fall"),
            AnimationType::Rise => write!(f, "rise"),
            AnimationType::Die => write!(f, "die"),
            AnimationType::Shoot => write!(f, "shoot"),
            AnimationType::ShootCrouched => write!(f, "shoot_crouched"),
        }
    }
}
