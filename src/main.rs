mod common;
mod input;
mod physics_and_collisions;
mod player;
mod enemy;
mod level;
mod telemetries;

use bevy::{prelude::*, window::WindowResolution};
use bevy_inspector_egui::{bevy_egui::EguiPlugin, DefaultInspectorConfigPlugin};
use common::camera::PlayerCameraPlugin;
use enemy::EnemyPlugin;
use input::InputPlugin;
use level::LevelPlugin;
use micro_banimate::BanimatePluginGroup;
use physics_and_collisions::CollisionsAndPhysicsPlugin;
use player::PlayerPlugin;
use telemetries::inspector_ui;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::BLACK))
        .add_plugins(DefaultPlugins
            .set(AssetPlugin {
                watch_for_changes_override: Some(true),
                mode: AssetMode::Unprocessed,
                ..default()
            })
            .set(WindowPlugin {
                primary_window: Some(Window {
                    title: String::from("Phys Bevy Test 2D"),
                    resolution: WindowResolution::new(1280.0, 720.0),
                    ..default()
                }),
                ..default()
            })
        )
        .add_plugins(EguiPlugin)
        .add_plugins(DefaultInspectorConfigPlugin)
        .add_plugins(CollisionsAndPhysicsPlugin)
        .add_plugins(BanimatePluginGroup)
        .add_plugins(InputPlugin)
        .add_plugins(PlayerCameraPlugin)
        .add_plugins(LevelPlugin)
        .add_plugins(PlayerPlugin)
        .add_plugins(EnemyPlugin)
        .add_systems(Update, inspector_ui)
        .run();
}
